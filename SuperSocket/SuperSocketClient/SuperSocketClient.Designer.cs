﻿namespace SuperSocketClient
{
    partial class SuperSocketClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.btn_StartConnect = new System.Windows.Forms.Button();
            this.btn_send = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(40, 67);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(721, 316);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = "";
            // 
            // btn_StartConnect
            // 
            this.btn_StartConnect.Location = new System.Drawing.Point(266, 13);
            this.btn_StartConnect.Margin = new System.Windows.Forms.Padding(4);
            this.btn_StartConnect.Name = "btn_StartConnect";
            this.btn_StartConnect.Size = new System.Drawing.Size(100, 29);
            this.btn_StartConnect.TabIndex = 3;
            this.btn_StartConnect.Text = "连接";
            this.btn_StartConnect.UseVisualStyleBackColor = true;
            this.btn_StartConnect.Click += new System.EventHandler(this.btn_StartConnect_Click);
            // 
            // btn_send
            // 
            this.btn_send.Location = new System.Drawing.Point(404, 13);
            this.btn_send.Margin = new System.Windows.Forms.Padding(4);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(100, 29);
            this.btn_send.TabIndex = 5;
            this.btn_send.Text = "发送数据";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // SuperSocketClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_send);
            this.Controls.Add(this.btn_StartConnect);
            this.Controls.Add(this.richTextBox1);
            this.Name = "SuperSocketClient";
            this.Text = "SuperSocketClient";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button btn_StartConnect;
        private System.Windows.Forms.Button btn_send;
    }
}