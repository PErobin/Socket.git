﻿using System;
using System.Text;
using SuperSocket.ClientEngine;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace SuperSocketClient
{
    public class SuperScoketClient
    {
        
        AsyncTcpSession async;
        /// <summary>
        ///
        /// </summary>
        /// <param name="ip">服务器IP</param>
        /// <param name="port">服务器端口</param>
        public SuperScoketClient(string ip, int port)
        {

            async.Connect(new IPEndPoint(IPAddress.Parse(ip), port));
            // 连接断开事件
            async.Closed += client_Closed;
            // 收到服务器数据事件
            async.DataReceived += client_DataReceived;
            // 连接到服务器事件
            async.Connected += client_Connected;
            // 发生错误的处理
            async.Error += client_Error;
        }
        void client_Error(object sender, ErrorEventArgs e)
        {
            Console.WriteLine(e.Exception.Message);
        }

        void client_Connected(object sender, EventArgs e)
        {
            Console.WriteLine("连接成功");
        }

        void client_DataReceived(object sender, DataEventArgs e)
        {
            string msg = Encoding.Default.GetString(e.Data);
            Console.WriteLine(msg);
        }

        void client_Closed(object sender, EventArgs e)
        {
            Console.WriteLine("连接断开");
        }
        
    }
}
