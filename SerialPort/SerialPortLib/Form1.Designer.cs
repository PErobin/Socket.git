﻿namespace SerialPortLibTool
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_SerialInit = new System.Windows.Forms.Button();
            this.btn_SendData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_SerialInit
            // 
            this.btn_SerialInit.Location = new System.Drawing.Point(161, 66);
            this.btn_SerialInit.Name = "btn_SerialInit";
            this.btn_SerialInit.Size = new System.Drawing.Size(75, 23);
            this.btn_SerialInit.TabIndex = 0;
            this.btn_SerialInit.Text = "串口初始化";
            this.btn_SerialInit.UseVisualStyleBackColor = true;
            this.btn_SerialInit.Click += new System.EventHandler(this.btn_SerialInit_Click);
            // 
            // btn_SendData
            // 
            this.btn_SendData.Location = new System.Drawing.Point(161, 200);
            this.btn_SendData.Name = "btn_SendData";
            this.btn_SendData.Size = new System.Drawing.Size(75, 23);
            this.btn_SendData.TabIndex = 1;
            this.btn_SendData.Text = "发送消息";
            this.btn_SendData.UseVisualStyleBackColor = true;
            this.btn_SendData.Click += new System.EventHandler(this.btn_SendData_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_SendData);
            this.Controls.Add(this.btn_SerialInit);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_SerialInit;
        private System.Windows.Forms.Button btn_SendData;
    }
}

