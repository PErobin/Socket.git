﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SerialPortLib;
using static SerialPortLib.SerialPortInput;

namespace SerialPortLibTool
{
    public partial class Form1 : Form
    {
        SerialPortLib.SerialPortInput serialPortInput = new SerialPortInput();
        public Form1()
        {
            InitializeComponent();
            
            
            

        }
        void SerialPortInput_ConnectStatusChange(object sender, ConnectionStatusChangedEventArgs args)
        {
            if(serialPortInput.IsConnected)
            {
                MessageBox.Show("Serial Open");
            }
            else
            {
                MessageBox.Show("Serial Close");
            }
        }
        void SerialPortInput_MessageReceive(object sender, MessageReceivedEventArgs args)
        {
            string a = System.Text.Encoding.UTF8.GetString(args.Data);
            MessageBox.Show(a);
        }

        private void btn_SerialInit_Click(object sender, EventArgs e)
        {
            try
            {
                serialPortInput.SetPort("COM1", 9600, StopBits.One, Parity.None,DataBits.Eight);//初始化串口:串口名，波特率，停止位，奇偶校验，数据位
                serialPortInput.Connect();
                serialPortInput.ConnectionStatusChanged += SerialPortInput_ConnectStatusChange;
                serialPortInput.MessageReceived += SerialPortInput_MessageReceive;
            }
            catch
            {
                MessageBox.Show("串口初始化失败");
            }
            
        }

        private void btn_SendData_Click(object sender, EventArgs e)
        {
            byte[] a = System.Text.Encoding.UTF8.GetBytes("1234");
            serialPortInput.SendMessage(a);
        }
    }
}
