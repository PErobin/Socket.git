# Socket

## 介绍

用于测试C#平台通讯协议的仓库。

## 软件架构

SuperSocket：TCP的Socket通信，基于事件执行。

01_SerialPort：串口通信，基于微软提供的原生接口实现。

SerialPortLib:串口通信，基于SerialPortLib实现。

### SuperSocket

##### 调用方法（服务端举例）

1、初始化对象，并注册事件

```
            appServer = new AppServer();
            //SuperSocket自定义了三个事件 ,连接事件,接收事件,关闭事件
            appServer.NewSessionConnected += appServer_NewSessionConnected;
            appServer.NewRequestReceived += appServer_NewRequestReceived;
            appServer.SessionClosed += appServer_SessionClosed;
```

2、启动服务

```
if (!appServer.Setup(int.Parse(txt_port.Text)))
            {
                SetMessage("Failed to Setup");
                return;
            }
            if (!appServer.Start())
            {
                SetMessage("Failed to Start");
                return;
            }
            else
            {
                SetMessage("开启监听");
            }
```

3、当服务端接受到客户端连接、客户端发送的消息、客户端断开连接后触发事件

#### SerialPortLib

1、初始化串口，并注册事件

```
SerialPortLib.SerialPortInput serialPortInput = new SerialPortInput();
            try
            {
                serialPortInput.SetPort("COM1", 9600, StopBits.One, Parity.None,DataBits.Eight);//初始化串口:串口名，波特率，停止位，奇偶校验，数据位
                serialPortInput.Connect();
                serialPortInput.ConnectionStatusChanged += SerialPortInput_ConnectStatusChange;
                serialPortInput.MessageReceived += SerialPortInput_MessageReceive;
            }
            catch
            {
                MessageBox.Show("串口初始化失败");
            }
```

2、事件执行

```
void SerialPortInput_ConnectStatusChange(object sender, ConnectionStatusChangedEventArgs args)
        {
            //断开和连接时执行
        }
        void SerialPortInput_MessageReceive(object sender, MessageReceivedEventArgs args)
        {
            //收到消息后触发
            string a = System.Text.Encoding.UTF8.GetString(args.Data);
            MessageBox.Show(a);
        }
```

## 开发环境

1、Visual Studio 2017

2、.netFrameWork 4.6

## 使用说明

1.  下载后可直接运行

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

## 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
